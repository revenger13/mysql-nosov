## About
This is instruction for manual setup of mysql database on Ubuntu server 20.04. Proof-of-work type of script, made for cloud-computing course

## Execution
On fresh Ubuntu 20.04:
```
sudo apt-get update
sudo apt install mysql-server
```
After this you should change configuration so your db could be accessed from network:
```
nano /etc/mysql/mysql.conf.d/mysqld.cnf
```
Change "bind-address = 127.0.0.1" to "bind-address = 0.0.0.0" and write to file. Next you should create new user with all permissions, accessible from network, because root is only for localhost user:
```
CREATE USER 'root2'@'%' IDENTIFIED BY 'password';
GRANT ALL PRIVILEGES ON *.* TO 'root2'@'%' WITH GRANT OPTION;
```
Script performs all of these tasks, using only shell syntax
## Access
At the end you should be able to connect from client device with
```
sudo mysql -h %host-ip% -u 'root2' -p
```
and then enter the default password "password"
